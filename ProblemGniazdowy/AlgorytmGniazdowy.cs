﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProblemGniazdowy
{
	public class AlgorytmGniazdowy
	{
		List<Zadanie> zadania = new List<Zadanie>();

		public void WczytajDane(string scierzkaDoPliku)
		{
			try
			{
				using (var input = new StreamReader(scierzkaDoPliku))
				{
					int liczbaZadan = int.Parse(input.ReadLine());
					zadania.Capacity = liczbaZadan;
					string[] tokensTech = input.ReadLine().Split();
					string[] tokensMasz = input.ReadLine().Split();
					string[] czasy = input.ReadLine().Split();

					for (int i = 0; i < liczbaZadan + 1; i++)
						zadania.Add(new Zadanie());
					for (int i = 1; i < liczbaZadan + 1; i++)
					{
						int n = int.Parse(tokensTech[i-1]);
						if (n >= 1)
							zadania[n].PoprzTech = i;
						n = int.Parse(tokensMasz[i-1]);
						if (n >= 1)
							zadania[n].PoprzMasz = i;
						zadania[i].CzasWykonania = int.Parse(czasy[i-1]);
					}

					int liczbaMaszyn = int.Parse(input.ReadLine());
					string[] tokens = input.ReadLine().Split();
					int nrMaszyny = 1;
					for (int i = 0; i < tokens.Length && nrMaszyny <= liczbaMaszyn; i++)
					{
						int n = int.Parse(tokens[i]);
						if (n != 0)
							zadania[n].NrMaszyny = nrMaszyny;
						else
							nrMaszyny++;
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
		}

		public int AlgGniazdowy()
		{
			int n = zadania.Count;
			int[,] pk = new int[2, n];
			bool[] wyznaczony = new bool[n];
			wyznaczony[0] = true;
			int liczbaWyznaczonych = 0;
			int Cmax = 0;
			for (int i = 0; i < n; i++)
				if (zadania[i].PoprzMasz == 0 && zadania[i].PoprzTech == 0)
				{
					pk[0, i] = 0;
					pk[1, i] = zadania[i].CzasWykonania;
					if (pk[1, i] > Cmax)
						Cmax = pk[1, i];
					wyznaczony[i] = true;
					liczbaWyznaczonych++;
				}
			if (liczbaWyznaczonych == 1)
				return -1;
			int j = 0;
			while (liczbaWyznaczonych != n)
			{
				int poprzMasz = zadania[j].PoprzMasz;
				int poprzTech = zadania[j].PoprzTech;
				if ((poprzMasz != 0 || poprzTech != 0) && wyznaczony[j] == false)
					if (wyznaczony[poprzMasz] == true && wyznaczony[poprzTech] == true)
					{
						wyznaczony[j] = true;
						liczbaWyznaczonych++;
						int w = Math.Max(pk[1, poprzTech], pk[1, poprzMasz]);
						pk[0, j] = w;
						pk[1, j] = w + zadania[j].CzasWykonania;
						if (pk[1, j] > Cmax)
							Cmax = pk[1, j];
					}
				j++;
				if (j == n) j = 0;
			}

			return Cmax;
		}
	}
}
