﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProblemGniazdowy
{
	public class Zadanie
	{
		public int NrZadania { get; set; }
		public int PoprzTech { get; set; }
		public int PoprzMasz { get; set; }
		public int CzasWykonania { get; set; }
		public int NrMaszyny { get; set; }
	}
}
